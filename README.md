# apartment-search-service project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

To run the application in dev mode run:

```shell script
$ ./mvnw compile quarkus:dev
```

> **_NOTE:_**  Access Swagger UI, which is available in dev mode only
> at http://localhost:8080/q/swagger-ui/.

## Tests

To run tests and produce code coverage report under `/target/jacoco-report` run:

```shell script
$ ./mvnw clean verify
```

> **_NOTE:_** Alternatively you can run the application in dev mode and press [r] to run live
> tests (no test coverage report).

## Database

By default, Quarkus uses `devservices` in dev mode which will take care of downloading the
database's docker image and configure the app to connect to it automatically.

If however you want
to spin up a database and configure the app to connect to it, you can run:

```shell script
$ docker-compose up -d
$ cp .env.example .env
```

> **_NOTE:_**  Database gets initialized with data from import.sql which is under src/main/resources
> and the app is configured to drop-and-create collections every time it is started in dev mode

