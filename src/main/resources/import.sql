insert into apartment(id, availability, location, squareMeters, price)
values (1, 'Sale', 'Athens', 100, 230000);
insert into apartmentType(apartment_id, id, type)
values (1, 1, 'Apartment');

insert into apartment(id, availability, location, squareMeters, price)
values (2, 'Sale', 'Athens', 70, 190000);
insert into apartmentType(apartment_id, id, type)
values (2, 2, 'Apartment');
insert into apartmentType(apartment_id, id, type)
values (2, 3, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (3, 'Sale', 'Thessalloniki', 140, 250000);
insert into apartmentType(apartment_id, id, type)
values (3, 4, 'Apartment');

insert into apartment(id, availability, location, squareMeters, price)
values (4, 'Rent', 'Patra', 55, 250);
insert into apartmentType(apartment_id, id, type)
values (4, 5, 'Apartment');

insert into apartment(id, availability, location, squareMeters, price)
values (5, 'Rent', 'Patra', 80, 360);
insert into apartmentType(apartment_id, id, type)
values (5, 6, 'Apartment');
insert into apartmentType(apartment_id, id, type)
values (5, 7, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (6, 'Sale', 'Athens', 30, 9000);
insert into apartmentType(apartment_id, id, type)
values (6, 8, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (7, 'Sale', 'Athens', 40, 120000);
insert into apartmentType(apartment_id, id, type)
values (7, 9, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (8, 'Rent', 'Thessalloniki', 65, 300);
insert into apartmentType(apartment_id, id, type)
values (8, 10, 'Studio');
insert into apartmentType(apartment_id, id, type)
values (8, 11, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (9, 'Rent', 'Thessalloniki', 65, 300);
insert into apartmentType(apartment_id, id, type)
values (9, 12, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (10, 'Rent', 'Thessalloniki', 90, 500);
insert into apartmentType(apartment_id, id, type)
values (10, 13, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (11, 'Sale', 'Athens', 70, 350000);
insert into apartmentType(apartment_id, id, type)
values (11, 14, 'Loft');
insert into apartmentType(apartment_id, id, type)
values (11, 15, 'Apartment');
insert into apartmentType(apartment_id, id, type)
values (11, 16, 'Studio');

insert into apartment(id, availability, location, squareMeters, price)
values (12, 'Sale', 'Chania', 45, 20000);
insert into apartmentType(apartment_id, id, type)
values (12, 17, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (13, 'Sale', 'Patra', 55, 35000);
insert into apartmentType(apartment_id, id, type)
values (13, 18, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (14, 'Rent', 'Chania', 65, 400);
insert into apartmentType(apartment_id, id, type)
values (14, 19, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (15, 'Rent', 'Patra', 45, 250);
insert into apartmentType(apartment_id, id, type)
values (15, 20, 'Loft');

insert into apartment(id, availability, location, squareMeters, price)
values (16, 'Sale', 'Thessalloniki', 65, 140000);
insert into apartmentType(apartment_id, id, type)
values (16, 21, 'Maisonette');
insert into apartmentType(apartment_id, id, type)
values (16, 22, 'Apartment');

insert into apartment(id, availability, location, squareMeters, price)
values (17, 'Sale', 'Patra', 210, 340000);
insert into apartmentType(apartment_id, id, type)
values (17, 23, 'Maisonette');

insert into apartment(id, availability, location, squareMeters, price)
values (18, 'Rent', 'Thessalloniki', 120, 700);
insert into apartmentType(apartment_id, id, type)
values (18, 24, 'Maisonette');

insert into apartment(id, availability, location, squareMeters, price)
values (19, 'Rent', 'Patra', 100, 160);
insert into apartmentType(apartment_id, id, type)
values (19, 25, 'Maisonette');

insert into apartment(id, availability, location, squareMeters, price)
values (20, 'Rent', 'Patra', 100, 160);
insert into apartmentType(apartment_id, id, type)
values (20, 26, 'Maisonette');