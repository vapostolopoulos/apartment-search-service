package repository;

import entity.ApartmentType;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ApartmentTypeRepositoryImpl implements ApartmentTypeRepository, PanacheRepository<ApartmentType> {

    @Override
    public List<ApartmentType> get(String query) {
        return find(query).stream().collect(Collectors.toList());
    }
}
