package repository;

import entity.Apartment;

import java.util.List;

public interface ApartmentRepository {

    List<Apartment> get(String query);
}
