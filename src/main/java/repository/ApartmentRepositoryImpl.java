package repository;

import entity.Apartment;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ApartmentRepositoryImpl implements ApartmentRepository, PanacheRepository<Apartment> {

    @Override
    public List<Apartment> get(String query) {
        return find(query).stream().collect(Collectors.toList());
    }
}
