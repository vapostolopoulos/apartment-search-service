package repository;

import entity.ApartmentType;

import java.util.List;

public interface ApartmentTypeRepository {

    List<ApartmentType> get(String query);

}
