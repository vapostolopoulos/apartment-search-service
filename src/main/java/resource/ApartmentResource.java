package resource;

import entity.Apartment;
import model.Request;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.logging.Logger;
import service.ApartmentService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("/api/v1/apartments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ApartmentResource {

    private static final Logger LOGGER = Logger.getLogger(ApartmentResource.class);

    private final ApartmentService service;

    @Inject
    public ApartmentResource(ApartmentService service) {
        this.service = service;
    }

    @POST
    @Operation(
            summary = "Get available apartments meeting the search criteria",
            description = "Retrieve available apartments meeting the search criteria")
    @APIResponse(responseCode = "200", description = "Provides available apartments meeting the search criteria")
    @APIResponse(responseCode = "400", description = "Improperly formatted search criteria")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    public Response getApartments(@Valid Request request) {
        LOGGER.infof("Received: %s", request);

        final List<Apartment> apartments;
        try {
            apartments = service.getApartments(request);
        } catch (Exception e) {
            LOGGER.error("Exception caught: ", e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }

        LOGGER.infof("Fetched %s from database", apartments);

        return Response.status(Status.OK).entity(apartments).build();
    }

}
