package service;

import builder.ApartmentQueryBuilder;
import builder.ApartmentTypeQueryBuilder;
import entity.Apartment;
import model.Request;
import model.Type;
import org.jboss.logging.Logger;
import repository.ApartmentRepository;
import repository.ApartmentTypeRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class ApartmentServiceImpl implements ApartmentService {

    private static final Logger LOGGER = Logger.getLogger(ApartmentServiceImpl.class);

    private final ApartmentRepository apartmentRepository;
    private final ApartmentTypeRepository apartmentTypeRepository;
    private final ApartmentQueryBuilder apartmentQueryBuilder;
    private final ApartmentTypeQueryBuilder apartmentTypeQueryBuilder;

    @Inject
    public ApartmentServiceImpl(ApartmentRepository apartmentRepository, ApartmentTypeRepository apartmentTypeRepository,
            ApartmentQueryBuilder apartmentQueryBuilder, ApartmentTypeQueryBuilder apartmentTypeQueryBuilder) {
        this.apartmentRepository = apartmentRepository;
        this.apartmentTypeRepository = apartmentTypeRepository;
        this.apartmentQueryBuilder = apartmentQueryBuilder;
        this.apartmentTypeQueryBuilder = apartmentTypeQueryBuilder;
    }

    @Override
    public List<Apartment> getApartments(Request request) {

        final var query = buildQuery(request);

        LOGGER.infof("Will execute query to Apartment collection: \"%s\"", query);

        return apartmentRepository.get(query);
    }

    private String buildQuery(Request request) {
        final var queryWithCriteriaExceptTypeCriterion = apartmentQueryBuilder.buildQueryWithoutTypes(request);
        final var subQueryWithTypeCriterion = buildSubQueryBasedOnTypeCriterion(request.getTypes());

        return queryWithCriteriaExceptTypeCriterion + subQueryWithTypeCriterion;
    }

    private String buildSubQueryBasedOnTypeCriterion(Set<Type> types) {
        if (types == null) {
            return "";
        }

        final var queryForApartmentTypeCollection = apartmentTypeQueryBuilder.buildQuery(types);

        LOGGER.infof("Will execute query to ApartmentType collection: \"%s\"", queryForApartmentTypeCollection);

        final var apartmentTypes = apartmentTypeRepository.get(queryForApartmentTypeCollection);

        return apartmentQueryBuilder.buildSubQueryWithIdsCorrespondingToTypes(apartmentTypes);
    }

}