package service;

import entity.Apartment;
import model.Request;

import java.util.List;

public interface ApartmentService {

    List<Apartment> getApartments(Request request);

}
