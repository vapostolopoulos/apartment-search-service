package model;

public enum Type {
    APARTMENT,
    LOFT,
    MAISONETTE,
    STUDIO
}
