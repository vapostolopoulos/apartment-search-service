package model;

public enum Availability {
    SALE,
    RENT
}
