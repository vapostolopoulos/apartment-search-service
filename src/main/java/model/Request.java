package model;

import common.Util;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Set;

public class Request {

    private final Availability availability;
    private final Set<Location> locations;
    private final Set<Type> types;
    @Min(Util.PRICE_MIN)
    private final int priceMin;
    @Max(Util.PRICE_MAX)
    private final int priceMax;
    @Min(Util.SQUARE_METERS_MIN)
    private final int squareMetersMin;
    @Max(Util.SQUARE_METERS_MAX)
    private final int squareMetersMax;

    public Request(Availability availability, Set<Location> locations, Set<Type> types, int priceMin, int priceMax, int squareMetersMin, int squareMetersMax) {
        this.availability = availability;
        this.locations = locations;
        this.types = types;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
        this.squareMetersMin = squareMetersMin;
        this.squareMetersMax = squareMetersMax;
    }

    public Availability getAvailability() {
        return availability;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public int getPriceMin() {
        return priceMin;
    }

    public int getPriceMax() {
        return priceMax;
    }

    public int getSquareMetersMin() {
        return squareMetersMin;
    }

    public int getSquareMetersMax() {
        return squareMetersMax;
    }

    @Override
    public String toString() {
        return
                String.format("%s{availability:%s, locations:%s, types:%s, priceMin:%d, priceMax:%d, squareMetersMin:%d, squareMetersMax:%d}",
                        getClass().getSimpleName(), availability, locations, types, priceMin, priceMax, squareMetersMin, squareMetersMax);
    }
}
