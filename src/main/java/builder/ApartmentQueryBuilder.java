package builder;

import entity.ApartmentType;
import model.Request;

import java.util.List;

public interface ApartmentQueryBuilder {

    String buildQueryWithoutTypes(Request request);

    String buildSubQueryWithIdsCorrespondingToTypes(List<ApartmentType> apartmentTypes);

}
