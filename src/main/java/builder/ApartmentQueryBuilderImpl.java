package builder;

import common.Util;
import entity.ApartmentType;
import model.Availability;
import model.Location;
import model.Request;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ApartmentQueryBuilderImpl implements ApartmentQueryBuilder {

    @Override
    public String buildQueryWithoutTypes(Request request) {
        final var query = new StringBuilder();
        final var criteria = new ArrayList<String>();
        query.append("from Apartment");

        addAvailabilityCriterion(request.getAvailability(), criteria);
        addLocationsCriteria(request.getLocations(), criteria);
        addPriceMinCriterion(request.getPriceMin(), criteria);
        addPriceMaxCriterion(request.getPriceMax(), criteria);
        addSquareMetersMinCriterion(request.getSquareMetersMin(), criteria);
        addSquareMetersMaxCriterion(request.getSquareMetersMax(), criteria);

        if (!criteria.isEmpty()) {
            query.append(" where ");
            final var formattedCriteria = String.join(" and ", criteria);
            query.append(formattedCriteria);
        }

        return query.toString();
    }

    @Override
    public String buildSubQueryWithIdsCorrespondingToTypes(List<ApartmentType> apartmentTypes) {
        if (apartmentTypes.isEmpty()) {
            return "";
        }

        return " and (id=" + apartmentTypes.stream().map(apartmentType -> apartmentType.getApartment().getId().toString())
                .collect(Collectors.joining(" or id="))
                + ")";
    }

    private void addAvailabilityCriterion(Availability availability, List<String> criteria) {
        if (availability != null) {
            final var formattedAvailability = format(availability.toString());
            criteria.add(String.format("availability = %s", formattedAvailability));
        }
    }

    private void addLocationsCriteria(Set<Location> locations, List<String> criteria) {
        if (locations != null) {
            final var formattedLocations = locations.stream().map(location -> format(location.toString())).collect(Collectors.joining(", "));
            criteria.add(String.format("location in (%s)", formattedLocations));
        }
    }

    private void addPriceMinCriterion(int priceMin, List<String> criteria) {
        if (priceMin != Util.PRICE_MIN) {
            criteria.add(String.format("price >= %d", priceMin));
        }
    }

    private void addPriceMaxCriterion(int priceMax, List<String> criteria) {
        if (priceMax != Util.PRICE_MAX) {
            criteria.add(String.format("price <= %d", priceMax));
        }
    }

    private void addSquareMetersMinCriterion(int squareMetersMin, List<String> criteria) {
        if (squareMetersMin != Util.SQUARE_METERS_MIN) {
            criteria.add(String.format("squareMeters >= %d", squareMetersMin));
        }
    }

    private void addSquareMetersMaxCriterion(int squareMetersMax, List<String> criteria) {
        if (squareMetersMax != Util.SQUARE_METERS_MAX) {
            criteria.add(String.format("squareMeters <= %d", squareMetersMax));
        }
    }

    private String format(final String word) {
        return "'" + Character.toUpperCase(word.charAt(0)) + word.toLowerCase().substring(1) + "'";
    }
}
