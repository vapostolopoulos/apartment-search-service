package builder;

import model.Type;

import javax.enterprise.context.ApplicationScoped;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ApartmentTypeQueryBuilderImpl implements ApartmentTypeQueryBuilder {

    @Override
    public String buildQuery(Set<Type> types) {
        final var query = new StringBuilder();
        final var criterion = new StringBuilder();
        query.append("from ApartmentType");

        addTypeCriterion(types, criterion);

        if (criterion.length() != 0) {
            query.append(String.format(" where %s", criterion));
        }

        return query.toString();
    }

    private void addTypeCriterion(Set<Type> types, StringBuilder criteria) {
        if (types != null) {
            final var formattedTypes = types.stream().map(type -> format(type.toString())).collect(Collectors.joining(", "));
            criteria.append(String.format("type in (%s)", formattedTypes));
        }
    }

    private String format(final String word) {
        return "'" + Character.toUpperCase(word.charAt(0)) + word.toLowerCase().substring(1) + "'";
    }
}
