package builder;

import model.Type;

import java.util.Set;

public interface ApartmentTypeQueryBuilder {

    String buildQuery(Set<Type> types);

}
