package common;

public class Util {

    public static final int PRICE_MIN = 10;
    public static final int PRICE_MAX = 10000000;
    public static final int SQUARE_METERS_MIN = 20;
    public static final int SQUARE_METERS_MAX = 10000;

    private Util() {
    }
}
