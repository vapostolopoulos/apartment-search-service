package entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Apartment {

    private Long id;
    private Set<ApartmentType> apartmentTypes;
    private String availability;
    private String location;
    private int squareMeters;
    private int price;

    protected Apartment() {
    }

    public Apartment(Long id, Set<ApartmentType> apartmentTypes, String availability, String location, int squareMeters, int price) {
        this.id = id;
        this.apartmentTypes = apartmentTypes;
        this.availability = availability;
        this.location = location;
        this.squareMeters = squareMeters;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(int squareMeters) {
        this.squareMeters = squareMeters;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @OneToMany(mappedBy = "apartment")
    public Set<ApartmentType> getApartmentTypes() {
        return apartmentTypes;
    }

    public void setApartmentTypes(Set<ApartmentType> apartmentTypes) {
        this.apartmentTypes = apartmentTypes;
    }

    @Override
    public String toString() {
        return
                String.format("%s{id:%d, availability:%s, location:%s, price:%d, squareMeters:%d, types:%s}",
                        getClass().getSimpleName(), id, availability, location, price, squareMeters, apartmentTypes);
    }
}
