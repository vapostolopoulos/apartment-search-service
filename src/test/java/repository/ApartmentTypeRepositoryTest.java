package repository;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ApartmentTypeRepositoryTest {

    @Inject
    ApartmentTypeRepository repository;

    @Test
    void ensureConnectionToCollection() {
        final var apartmentTypes = repository.get("from ApartmentType");

        assertThat(apartmentTypes).hasSizeGreaterThanOrEqualTo(0);
    }
}
