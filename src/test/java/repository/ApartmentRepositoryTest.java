package repository;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ApartmentRepositoryTest {

    @Inject
    ApartmentRepository repository;

    @Test
    void ensureConnectionToCollection() {
        final var apartments = repository.get("from Apartment");

        assertThat(apartments).hasSizeGreaterThanOrEqualTo(0);
    }
}
