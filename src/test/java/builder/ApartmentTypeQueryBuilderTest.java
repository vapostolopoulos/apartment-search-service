package builder;

import io.quarkus.test.junit.QuarkusTest;
import model.Type;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ApartmentTypeQueryBuilderTest {

    @Inject
    ApartmentTypeQueryBuilder queryBuilder;

    @Test
    void shouldProduceAGetAllQueryUponRequestWithNullSetOfTypes() {
        final var expectedQuery = "from ApartmentType";

        final var query = queryBuilder.buildQuery(null);

        assertThat(query).isEqualTo(expectedQuery);
    }

    @Test
    void shouldProduceQueryWithTypeCriterion() {
        final var studio = Type.STUDIO;
        final var apartment = Type.APARTMENT;
        final var loft = Type.LOFT;
        final var maisonette = Type.MAISONETTE;
        final var types = Set.of(studio, maisonette, loft, apartment);

        final var expectedQuery = "from ApartmentType where type in ('Maisonette', 'Loft', 'Studio', 'Apartment')";

        final var query = queryBuilder.buildQuery(types);

        final var queriesAreEquivalent = equivalent(expectedQuery, query);

        assertThat(queriesAreEquivalent).isTrue();
    }

    private boolean equivalent(String s1, String s2) {
        final var chars1 = s1.toCharArray();
        Arrays.sort(chars1);

        final var chars2 = s2.toCharArray();
        Arrays.sort(chars2);

        return Arrays.equals(chars1, chars2);
    }
}
