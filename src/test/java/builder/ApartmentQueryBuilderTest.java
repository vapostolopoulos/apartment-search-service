package builder;

import common.Util;
import entity.Apartment;
import entity.ApartmentType;
import io.quarkus.test.junit.QuarkusTest;
import model.Availability;
import model.Location;
import model.Request;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ApartmentQueryBuilderTest {

    @Inject
    ApartmentQueryBuilder queryBuilder;

    @Test
    void shouldProduceAGetAllQueryUponRequestWithNoCriteriaButTheRequiredProvided() {
        final var request = new Request(null, null, null, Util.PRICE_MIN, Util.PRICE_MAX, Util.SQUARE_METERS_MIN, Util.SQUARE_METERS_MAX);
        final var expectedQuery = "from Apartment";

        final var query = queryBuilder.buildQueryWithoutTypes(request);

        assertThat(query).isEqualTo(expectedQuery);
    }

    @Test
    void shouldProduceQueryWithCriteriaUponRequestWithAllCriteriaPresentInApartmentCollection() {
        final var locations = new HashSet<>(Arrays.asList(Location.ATHENS, Location.CHANIA));
        final var request = new Request(Availability.RENT, locations, null, 200, 350, 30, 50);
        final var expectedQuery = "from Apartment where availability = 'Rent' and location in ('Athens', 'Chania') and price >= 200 and price <= 350"
                + " and squareMeters >= 30 and squareMeters <= 50";

        final var query = queryBuilder.buildQueryWithoutTypes(request);

        final var queriesAreEquivalent = equivalent(expectedQuery, query);

        assertThat(queriesAreEquivalent).isTrue();
    }

    @Test
    void shouldProvideEmptySubQueryUponEmptyListOfApartmentTypes() {
        final var expectedSubQuery = "";

        final var subQuery = queryBuilder.buildSubQueryWithIdsCorrespondingToTypes(List.of());

        assertThat(expectedSubQuery).isEqualTo(subQuery);
    }

    @Test
    void shouldProvideSubQueryWithIdsCorrespondingToTypes() {
        final var apartment1 = new Apartment(17L, null, "Sale", "Athens", 100, 1000);
        final var loft1 = new ApartmentType(79L, "Loft", apartment1);
        apartment1.setApartmentTypes(Set.of(loft1));

        final var apartment2 = new Apartment(42L, null, "Sale", "Athens", 100, 1000);
        final var loft2 = new ApartmentType(56L, "Loft", apartment2);
        apartment2.setApartmentTypes(Set.of(loft2));

        final var apartmentTypes = List.of(loft1, loft2);

        final var expectedSubQuery = " and (id=42 or id=17)";

        final var subQuery = queryBuilder.buildSubQueryWithIdsCorrespondingToTypes(apartmentTypes);

        final var queriesAreEquivalent = equivalent(expectedSubQuery, subQuery);

        assertThat(queriesAreEquivalent).isTrue();
    }

    private boolean equivalent(String s1, String s2) {
        final var chars1 = s1.toCharArray();
        Arrays.sort(chars1);

        final var chars2 = s2.toCharArray();
        Arrays.sort(chars2);

        return Arrays.equals(chars1, chars2);
    }
}
