package service;

import builder.ApartmentQueryBuilder;
import builder.ApartmentTypeQueryBuilder;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import model.Availability;
import model.Location;
import model.Request;
import model.Type;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import repository.ApartmentRepository;
import repository.ApartmentTypeRepository;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashSet;

@QuarkusTest
class ApartmentServiceTest {

    @Inject
    ApartmentService service;
    @InjectMock
    ApartmentRepository apartmentRepository;
    @InjectMock
    ApartmentTypeRepository apartmentTypeRepository;
    @InjectMock
    ApartmentQueryBuilder apartmentQueryBuilder;
    @InjectMock
    ApartmentTypeQueryBuilder apartmentTypeQueryBuilder;

    @Test
    void shouldOnlyCallApartmentRepositoryAndQueryBuilderUponRequestWithoutTypes() {
        final var locations = new HashSet<>(Collections.singleton(Location.ATHENS));
        final var request = new Request(Availability.RENT, locations, null, 10, 10000000, 20, 10000);

        service.getApartments(request);

        Mockito.verify(apartmentRepository, Mockito.times(1)).get(Mockito.anyString());
        Mockito.verify(apartmentQueryBuilder, Mockito.times(1)).buildQueryWithoutTypes(request);
        Mockito.verify(apartmentQueryBuilder, Mockito.times(0)).buildSubQueryWithIdsCorrespondingToTypes(Mockito.anyList());

        Mockito.verify(apartmentTypeRepository, Mockito.times(0)).get(Mockito.anyString());
        Mockito.verify(apartmentTypeQueryBuilder, Mockito.times(0)).buildQuery(Mockito.anySet());
    }

    @Test
    void shouldCallApartmentAndApartmentTypeRepositoriesAndQueryBuildersUponRequestWithTypes() {
        final var locations = new HashSet<>(Collections.singleton(Location.ATHENS));
        final var types = new HashSet<Type>(Collections.singleton(Type.STUDIO));
        final var request = new Request(Availability.RENT, locations, types, 10, 10000000, 20, 10000);

        Mockito.when(apartmentTypeQueryBuilder.buildQuery(types)).thenReturn("");

        service.getApartments(request);

        Mockito.verify(apartmentRepository, Mockito.times(1)).get(Mockito.anyString());
        Mockito.verify(apartmentQueryBuilder, Mockito.times(1)).buildQueryWithoutTypes(request);
        Mockito.verify(apartmentQueryBuilder, Mockito.times(1)).buildSubQueryWithIdsCorrespondingToTypes(Mockito.anyList());

        Mockito.verify(apartmentTypeRepository, Mockito.times(1)).get(Mockito.anyString());
        Mockito.verify(apartmentTypeQueryBuilder, Mockito.times(1)).buildQuery(Mockito.anySet());
    }
}
