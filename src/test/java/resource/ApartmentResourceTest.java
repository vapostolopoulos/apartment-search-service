package resource;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import net.joshka.junit.json.params.JsonFileSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.Mockito;
import service.ApartmentService;

import javax.json.JsonObject;
import javax.ws.rs.core.Response.Status;

@QuarkusTest
@TestHTTPEndpoint(ApartmentResource.class)
class ApartmentResourceTest {

    @InjectMock
    ApartmentService service;

    @BeforeEach
    void enableRestAssuredLoggingIfValidationFails() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    void shouldReturn400UponEmptyRequestBody() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body("{}")
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/request-with-invalid-enum.json")
    void shouldReturn400UponInvalidEnum(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/request-with-invalid-priceMin.json")
    void shouldReturn400UponInvalidPriceMin(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/request-with-invalid-priceMax.json")
    void shouldReturn400UponInvalidPriceMax(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/request-with-invalid-squareMetersMin.json")
    void shouldReturn400UponInvalidSquareMetersMin(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/request-with-invalid-squareMetersMax.json")
    void shouldReturn400UponInvalidSquareMetersMax(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/valid-request.json")
    void shouldReturn500UponExceptionThrown(JsonObject request) {
        final var requestSerialized = request.toString();

        final var dummyRuntimeException = new RuntimeException("dummyRuntimeException");
        Mockito.doThrow(dummyRuntimeException).when(service).getApartments(Mockito.any());

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    @ParameterizedTest
    @JsonFileSource(resources = "/valid-request.json")
    void shouldReturn200UponValidRequest(JsonObject request) {
        final var requestSerialized = request.toString();

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestSerialized)
                .when()
                .post()
                .then()
                .statusCode(Status.OK.getStatusCode());
    }
}
